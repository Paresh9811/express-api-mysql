const mySql = require('mysql')
require('dotenv').config()

// Create a connection to the database
const connection = mySql.createConnection({
    connectionLimit: 10,
    host: process.env.HOST,
    user: process.env.USER,
    database: process.env.DATABASE
});

// open the MySQL connection
connection.connect(error => {
  if (error) throw error;
  console.log("Successfully connected to the database....");
});

module.exports = connection;