const express =  require('express');
const mySql = require ('mysql');
const bodyParser = require ('body-parser');
const route = require('./routes')
const app = express()
require('dotenv').config()

app.use(bodyParser.urlencoded({ extended : false}))
app.use(bodyParser.json())

//API Setting
app.use('/database/' , route.app)

//Port setting
const port = process.env.PORT
app.listen(port , () => console.log(`Litsening to port ${port}...`))