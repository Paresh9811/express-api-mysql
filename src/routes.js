const studentController = require('./students/controller');
const express = require('express');
const { studentValidate } = require('./students/model');
const app = express.Router()

//Get All students record API End point
app.get('/getAllStudent' , studentController.getStudent);

// //Get students record by ID  API End point
app.get('/getStudent/:id' , studentController.getStudentById);

// //DELET students record by ID  API End point
app.delete('/:id' , studentController.deletStudent)

// //Add students record   API End point
app.post('/addStudent'  , studentController.createStudent)

// //Update students record   API End point
app.patch('/updateStudent/:id' , studentController.updateStudent)

module.exports = {app}