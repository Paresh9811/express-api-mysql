const Connection = require('../connection');

//Create student model
const creatModelStudent = async (fields) => { 
	await Connection.query('INSERT INTO students  SET ?', [fields] , async (err , rows) => {
		if(!err){
			return await rows;
		}else{
			console.log(err)
		}
	})
}

//Get All Student Model.
const getAllStudentModel = (res) => {
	Connection.query('SELECT * FROM students', (err, rows) => {
		if (err) {
			console.log(err)
		}else{
			res.send(rows)

		}
	})	
}

// Get Stuident By Id.
const getStudentByIdModel = (req , res) => {
	// const params = req.body.id
	Connection.query('SELECT * FROM students WHERE ID = ?', [req.params.id], (err, rows) => {
		if(err){
			res.send(err)
		}
		if (rows.length <= 0){
			res.send('Sorry Given Id Is Not Found..')
		}else{
			res.send(rows)
		}
	})
}

// Delete record By ID router controller
const deletStudentByIdModel = (res, req) => {
	Connection.query('DELETE FROM students WHERE ID = ?', [req.params.id], (err, rows) => {
		if (err) {
			res.send(err)
		} if(rows.affectedRows == 0) {
			res.send('Given ID is not Found in Data Base...')
		}else{
			res.send(`Given ID : ${req.params.id} has been deleted succesfully`)
		}
	})
}

// Update Student record Router controller
const updateStudentModel = (req , res) => {
	// const { id, name, email, city, postalcode } = req.params.body
	// console.log(res)
	Connection.query('UPDATE students SET name = ?, email = ? ,city = ? , postalcode = ? WHERE id = ? ', [req.body.name ,req.body.email, req.body.city, req.body.postalcode , req.params.id], (err, rows) => {
		// console.log(err)
		if (err) {
			res.send(err)
		}if(rows.affectedRows == 0){
			res.send(`You are Entered ID : ${req.params.id} is Invalid ID`)
		} else {
			// res.send(req.body)
			res.send(`ID Nomber : ${req.params.id} Is succesfully updated into DataBAse.  HAve a good day ${req.body.name}..`)
		}
	})
}

//  Exporting All Model.
module.exports = {
	creatModelStudent,
	getAllStudentModel,
	getStudentByIdModel,
	deletStudentByIdModel,
	updateStudentModel
}