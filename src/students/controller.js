const Joi = require('joi');
const Connection = require('../connection');
const model = require('../students/model')

//Post Student record  router controller
const createStudent = async (req, res) => {
	const schema = Joi.object({
		name 		: Joi.string().min(3).max(15).required(),
		email 		: Joi.string().min(5).max(50).required().email(),
		city 		: Joi.string().min(2).max(20).required(),
		postalcode 	: Joi.string().min(2).max(10).required()
	})

	const {value:fields,error:error} = schema.validate(req.body)
	if(!error){
		await model.creatModelStudent(fields)
		res.status(200).send("You are succesfully added into database..")
	}else{
		res.send(error.message)
	}
}

//Get record of All Students Ruter controller...
const getStudent = (req, res) =>{
	 model.getAllStudentModel(res)
}

//Get Student record By ID ...
const getStudentById = async (req, res) => {
	model.getStudentByIdModel(req ,res)
}

//  Delete record By ID router controller
const deletStudent = async (req, res) =>{
	model.deletStudentByIdModel(res , req)
}

// Update Student record Router controller 
const updateStudent = async (req , res) => {
	model.updateStudentModel(req, res)
	
}

// Exporting All Module...
module.exports = {
	createStudent,
	getStudent,
	getStudentById,
	deletStudent,
	updateStudent
}